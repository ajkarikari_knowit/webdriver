/*
These test hooks require that this file is stored in the same folder as the project
check var browser before comencing selenium tests
check var pageURL
check var pageTitle
USAGE:
node test_hook.js

//var pageUrl = 'http://localhost:8080/startbootstrap/index.html';
//var pageTitle = 'Agency - Start Bootstrap Theme';
*/

'use strict';

var browser = 'firefox';
var pageUrl = 'http://www.ikea.com/se/sv';
var pageTitle = 'IKEA - Möbler, inredning och inspiration - IKEA';
console.log('Beginning tests');


var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

var driver = new webdriver.Builder()
    .forBrowser(browser)
    .build();
/*
driver.get(pageUrl);

driver.wait(check_title, 1000);
//driver.wait(start_planner, 3000);
//driver.wait(do_search, 3000);
//driver.wait(goto_product, 4000);
//driver.wait(buy_product, 5000);
//driver.wait(go_to_cart, 6000);


function check_title() {
    var functionName = 'Get page title: ';
    var promise = driver.getTitle().then(function (title) {
        if (title === pageTitle) {
            console.log(functionName + 'success');
            return true;
        } else {
            console.log(functionName + 'fail -- ' + title)
        }
    });
    return promise;
}

function start_planner() {
    var promise = driver.findElement(By.className('headZeroWhite')).then(function (webElement) {
        var functionName = 'Start planner: ';
        if (webElement) {
            driver.findElement(webdriver.By.className('headZeroWhite')).click();
            console.log(functionName + 'success');
            return true;
        } else {
            console.log(functionName + 'fail --')
        }
    });
    return promise;
}

function do_search() {
    var promise = driver.findElement(webdriver.By.id('search')).then(function (searchElement) {
        var functionName = 'Complete search: ';
        if (searchElement) {
            driver.findElement(webdriver.By.id('search')).sendKeys('malm serie');
            driver.findElement(webdriver.By.id('search')).sendKeys(webdriver.Key.ENTER);
            console.log(functionName + 'success');
            return true;
        } else {
            console.log(functionName + 'fail');
        }
    });
    return promise;
    //driver.findElement({ id: 'search' }).sendKeys(webdriver.Key.ENTER);
}

function goto_product() {
    var promise = driver.findElement(webdriver.By.id('item_50214555_2')).then(function (productElement) {
        var functionName = 'Go to product: ';
        if (productElement) {
            driver.findElement(webdriver.By.id('item_50214555_2')).click();
            console.log(functionName + 'success');
            return true;
        } else {
            console.log(functionName + 'fail');
        }
    });
    return promise;
}

function buy_product() {
    var promise = driver.findElement(webdriver.By.id('jsButton_buyOnline_lnk')).then(function (webElement) {
        var functionName = 'Buy product: ';
        if (webElement) {
            driver.findElement(webdriver.By.id('jsButton_buyOnline_lnk')).click();
            // var shoppinglistStyle = driver.findElement(webdriver.By.className('shoppinglistStyle'));
            // shoppinglistStyle = driver.findElement(webdriver.By.className('link')).click();
            console.log(functionName + 'success');
            return true;
        } else {
            console.log(functionName + 'fail')
        }
    });
    return promise;
}

function go_to_cart() {
    var promise = driver.findElement(webdriver.By.id('link_header_shopping_cart')).then(function (webElement) {
        var functionName = 'Go to cart: ';
        if (webElement) {
            driver.findElement(webdriver.By.id('link_header_shopping_cart')).click();
            driver.findElement(webdriver.By.id('jsButton_beginCheckOut_01')).click();
            console.log(functionName + 'success');
            return true;
        } else {
            console.log(functionName + 'fail');
        }
    });
    return promise;
}

//driver.quit(); */
